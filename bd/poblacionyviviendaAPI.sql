--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.4
-- Dumped by pg_dump version 9.3.4
-- Started on 2014-08-08 14:19:03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 184 (class 3079 OID 11750)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2014 (class 0 OID 0)
-- Dependencies: 184
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 183 (class 1259 OID 43006)
-- Name: censo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE censo (
    id_indicador integer NOT NULL,
    id_municipio integer,
    id_tema1 integer,
    id_tema2 integer,
    id_tema3 integer,
    anio double precision
);


ALTER TABLE public.censo OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 43004)
-- Name: censo_id_indicador_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE censo_id_indicador_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.censo_id_indicador_seq OWNER TO postgres;

--
-- TOC entry 2015 (class 0 OID 0)
-- Dependencies: 182
-- Name: censo_id_indicador_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE censo_id_indicador_seq OWNED BY censo.id_indicador;


--
-- TOC entry 171 (class 1259 OID 42809)
-- Name: entidad; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE entidad (
    id_entidad integer NOT NULL,
    descripcion character varying(200)
);


ALTER TABLE public.entidad OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 42807)
-- Name: entidad_id_entidad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE entidad_id_entidad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.entidad_id_entidad_seq OWNER TO postgres;

--
-- TOC entry 2016 (class 0 OID 0)
-- Dependencies: 170
-- Name: entidad_id_entidad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE entidad_id_entidad_seq OWNED BY entidad.id_entidad;


--
-- TOC entry 173 (class 1259 OID 42863)
-- Name: indicadores; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE indicadores (
    id_indicador integer NOT NULL,
    descripcion character varying(500),
    nota character varying(500)
);


ALTER TABLE public.indicadores OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 42861)
-- Name: indicadores_id_indicador_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE indicadores_id_indicador_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.indicadores_id_indicador_seq OWNER TO postgres;

--
-- TOC entry 2017 (class 0 OID 0)
-- Dependencies: 172
-- Name: indicadores_id_indicador_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE indicadores_id_indicador_seq OWNED BY indicadores.id_indicador;


--
-- TOC entry 181 (class 1259 OID 42993)
-- Name: municipio; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE municipio (
    id_municipio integer NOT NULL,
    id_entidad integer,
    descripcion character varying(200)
);


ALTER TABLE public.municipio OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 42991)
-- Name: municipio_id_municipio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE municipio_id_municipio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.municipio_id_municipio_seq OWNER TO postgres;

--
-- TOC entry 2018 (class 0 OID 0)
-- Dependencies: 180
-- Name: municipio_id_municipio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE municipio_id_municipio_seq OWNED BY municipio.id_municipio;


--
-- TOC entry 175 (class 1259 OID 42959)
-- Name: tema1; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tema1 (
    id_tema1 integer NOT NULL,
    descripcion character varying(200)
);


ALTER TABLE public.tema1 OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 42957)
-- Name: tema1_id_tema1_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tema1_id_tema1_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tema1_id_tema1_seq OWNER TO postgres;

--
-- TOC entry 2019 (class 0 OID 0)
-- Dependencies: 174
-- Name: tema1_id_tema1_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tema1_id_tema1_seq OWNED BY tema1.id_tema1;


--
-- TOC entry 177 (class 1259 OID 42967)
-- Name: tema2; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tema2 (
    id_tema2 integer NOT NULL,
    id_tema1 integer,
    descripcion character varying(200)
);


ALTER TABLE public.tema2 OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 42965)
-- Name: tema2_id_tema2_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tema2_id_tema2_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tema2_id_tema2_seq OWNER TO postgres;

--
-- TOC entry 2020 (class 0 OID 0)
-- Dependencies: 176
-- Name: tema2_id_tema2_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tema2_id_tema2_seq OWNED BY tema2.id_tema2;


--
-- TOC entry 179 (class 1259 OID 42980)
-- Name: tema3; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tema3 (
    id_tema3 integer NOT NULL,
    id_tema2 integer,
    descripcion character varying(200)
);


ALTER TABLE public.tema3 OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 42978)
-- Name: tema3_id_tema3_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tema3_id_tema3_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tema3_id_tema3_seq OWNER TO postgres;

--
-- TOC entry 2021 (class 0 OID 0)
-- Dependencies: 178
-- Name: tema3_id_tema3_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tema3_id_tema3_seq OWNED BY tema3.id_tema3;


--
-- TOC entry 1866 (class 2604 OID 43009)
-- Name: id_indicador; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY censo ALTER COLUMN id_indicador SET DEFAULT nextval('censo_id_indicador_seq'::regclass);


--
-- TOC entry 1860 (class 2604 OID 42812)
-- Name: id_entidad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY entidad ALTER COLUMN id_entidad SET DEFAULT nextval('entidad_id_entidad_seq'::regclass);


--
-- TOC entry 1861 (class 2604 OID 42866)
-- Name: id_indicador; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY indicadores ALTER COLUMN id_indicador SET DEFAULT nextval('indicadores_id_indicador_seq'::regclass);


--
-- TOC entry 1865 (class 2604 OID 42996)
-- Name: id_municipio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY municipio ALTER COLUMN id_municipio SET DEFAULT nextval('municipio_id_municipio_seq'::regclass);


--
-- TOC entry 1862 (class 2604 OID 42962)
-- Name: id_tema1; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tema1 ALTER COLUMN id_tema1 SET DEFAULT nextval('tema1_id_tema1_seq'::regclass);


--
-- TOC entry 1863 (class 2604 OID 42970)
-- Name: id_tema2; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tema2 ALTER COLUMN id_tema2 SET DEFAULT nextval('tema2_id_tema2_seq'::regclass);


--
-- TOC entry 1864 (class 2604 OID 42983)
-- Name: id_tema3; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tema3 ALTER COLUMN id_tema3 SET DEFAULT nextval('tema3_id_tema3_seq'::regclass);


--
-- TOC entry 2006 (class 0 OID 43006)
-- Dependencies: 183
-- Data for Name: censo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY censo (id_indicador, id_municipio, id_tema1, id_tema2, id_tema3, anio) FROM stdin;
1002000014	1	1	1	1	110610075
1002000015	1	1	1	1	107623589
1002000016	1	1	1	1	2841029
1002000017	1	1	1	1	145457
1002000018	1	1	1	1	28159373
1002000019	1	1	1	1	21243167
1002000020	1	1	1	1	6916206
1002000021	1	1	1	1	3.9300000000000002
1002000022	1	1	1	1	4.1100000000000003
1002000023	1	1	1	1	3.3599999999999999
1002000013	1	1	2	2	2.3399999999999999
1002000010	1	1	3	3	26
1002000011	1	1	3	3	25
1002000012	1	1	3	3	26
1002000024	1	1	3	3	95.430000000000007
1002000001	1	1	3	4	112336538
1002000002	1	1	3	4	54855231
1002000003	1	1	3	4	57481307
1003000001	1	1	4	5	28607568
1003000002	1	1	4	5	111954660
1003000003	1	1	4	5	103359676
1003000004	1	1	4	5	4670157
1003000005	1	1	4	5	1810853
1003000006	1	1	4	5	50924
1003000007	1	1	4	5	27231
1003000008	1	1	4	5	28814
1003000009	1	1	4	5	6446
1003000010	1	1	4	5	2000559
1003000011	1	1	4	5	28159373
1003000012	1	1	4	5	18745306
1003000013	1	1	4	5	8690747
1003000014	1	1	4	5	723320
1003000015	1	1	4	5	3.9300000000000002
1003000016	1	1	4	5	26224791
1003000017	1	1	4	6	27515030
1003000018	1	1	4	6	24808420
1003000019	1	1	4	6	25410351
1003000020	1	1	4	6	26848166
1003000021	1	1	4	6	26048531
1003000022	1	1	4	6	23091296
1003000023	1	1	4	6	18692852
1003000024	1	1	4	6	8279619
1005000039	1	2	5	7	6695228
1005000040	1	2	5	7	93203961
1005000041	1	2	5	7	511621
1002000041	1	2	6	7	92.349999999999994
1002000042	1	2	6	7	66.849999999999994
1005000001	1	2	6	7	98246031
1005000002	1	2	6	7	89599459
1005000003	1	2	6	7	43921560
1005000004	1	2	6	7	45677899
1005000005	1	2	6	7	7620948
1005000006	1	2	6	7	3289464
1005000007	1	2	6	7	4331484
1005000008	1	2	6	7	1025624
1005000009	1	2	6	7	495048
1005000010	1	2	6	7	530576
1005000011	1	2	6	7	100410810
1005000012	1	2	6	7	30482938
1005000013	1	2	6	7	15307914
1005000014	1	2	6	7	15175024
1005000015	1	2	6	7	68967761
1005000016	1	2	6	7	33046551
1005000017	1	2	6	7	35921210
1005000018	1	2	6	7	960111
1005000019	1	2	6	7	453604
1005000020	1	2	6	7	506507
1005000021	1	2	6	7	6019391
1005000022	1	2	6	7	2529348
1005000023	1	2	6	7	3490043
1005000024	1	2	6	7	4785991
1005000025	1	2	6	7	2434006
1005000026	1	2	6	7	2351985
1005000027	1	2	6	7	36467510
1005000028	1	2	6	7	17645963
1005000029	1	2	6	7	18821547
1005000030	1	2	6	7	52569119
1005000031	1	2	6	7	25923413
1005000032	1	2	6	7	26645706
1005000033	1	2	6	7	568799
1005000034	1	2	6	7	275339
1005000035	1	2	6	7	293460
1005000036	1	2	6	7	12061198
1005000037	1	2	6	7	897587
1005000038	1	2	6	7	8.6300000000000008
1004000001	1	2	7	8	72514513
1004000002	1	2	7	8	35380021
1004000003	1	2	7	8	7190494
1004000004	1	2	7	8	1091321
1004000005	1	2	7	8	29863496
1004000006	1	2	7	8	38020372
1004000007	1	2	7	8	1801653
1004000008	1	2	7	8	34418910
1004000009	1	2	7	8	38095603
\.


--
-- TOC entry 2022 (class 0 OID 0)
-- Dependencies: 182
-- Name: censo_id_indicador_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('censo_id_indicador_seq', 1, false);


--
-- TOC entry 1994 (class 0 OID 42809)
-- Dependencies: 171
-- Data for Name: entidad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY entidad (id_entidad, descripcion) FROM stdin;
1	Total nacional
\.


--
-- TOC entry 2023 (class 0 OID 0)
-- Dependencies: 170
-- Name: entidad_id_entidad_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('entidad_id_entidad_seq', 2, false);


--
-- TOC entry 1996 (class 0 OID 42863)
-- Dependencies: 173
-- Data for Name: indicadores; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY indicadores (id_indicador, descripcion, nota) FROM stdin;
1005000008	poblacion de 6 y mas aÃ±os que no especifico si sabe leer y escribir	no hay nota
1005000009	Hombres de 6 y mas aÃ±os que no especifican si saben leer y escribir	no hay nota
1005000010	Mujeres de 6 y mas aÃ±os que no especifican si saben leer y escribir	no hay nota
1002000010	Edad mediana	Se refiere a la edad expresada en aÃ±os y que divide a la poblacion en dos partes iguales, esto es, la edad hasta la cual se acumula el 50% de la poblacion total. Para el cÃ¡lculo se excluye la poblacion con edad no especificada.
1002000011	Edad mediana hombres	Se refiere a la edad expresada en aÃ±os y que divide a la poblacion en dos partes iguales, esto es, la edad hasta la cual se acumula el 50% de la poblacion total. Para el cÃ¡lculo se excluye la poblacion con edad no especificada.
1002000012	Edad mediana mujeres	Se refiere a la edad expresada en aÃ±os y que divide a la poblacion en dos partes iguales, esto es, la edad hasta la cual se acumula el 50% de la poblacion total. Para el cÃ¡lculo se excluye la poblacion con edad no especificada.
1002000024	Relacion hombres-mujeres	Hombres por cada 100 mujeres.
1005000011	poblacion de 5 y mas aÃ±os	no hay nota
1005000012	poblacion de 5 y mas aÃ±os que asiste a la escuela	no hay nota
1003000001	Total de viviendas particulares habitadas	Viviendas particulares habitadas de cualquier clase: casa independiente, departamento en edificio, vivienda o cuarto en vecindad, vivienda o cuarto de azotea, local no construido para habitacion, vivienda movil, refugios y clase no especificada. Incluye a las viviendas particulares sin informacion de ocupantes.
1003000002	Ocupantes en viviendas particulares	Comprende ocupantes en viviendas de: casa independiente, departamento en edificio, vivienda en vecindad, vivienda en cuarto de azotea, local no construido para habitacion, vivienda movil, refugio y ocupantes en viviendas particulares de clase no especificada.
1003000011	Viviendas particulares habitadas	Excluye viviendas moviles, refugios y locales no construidos para habitacion debido a que no se captaron caracterÃ­sticas de estas clases de vivienda. Excluye las viviendas sin informacion de ocupantes. Se refiere a viviendas con piso de cemento, firme, madera, mosaico u otro material. Excluye viviendas donde no se especifico el material predominante en pisos.
1003000016	Viviendas particulares habitadas con piso diferente de tierra	Excluye viviendas en locales no construidos para habitacion, viviendas moviles y refugios. Excluye viviendas sin informacion de ocupantes.
1003000017	Viviendas particulares habitadas que disponen de energÃ­a elÃ©ctrica	Excluye viviendas que no especificaron si disponen del servicio. Excluye viviendas sin informacion de ocupantes. Excluye viviendas en locales no construidos para habitacion, viviendas moviles y refugios.
1003000018	Viviendas particulares habitadas que disponen de agua de la red pÃºblica en el Ã¡mbito de la vivienda	Excluye viviendas sin informacion de ocupantes. Comprende viviendas que disponen de agua de la red del servicio pÃºblico dentro de la vivienda y fuera de ella pero dentro del terreno. Excluye viviendas en locales no construidos para habitacion, viviendas moviles y refugios. Excluye viviendas que no especificaron si disponen del servicio.
1003000019	Viviendas particulares habitadas que disponen de drenaje	Excluye viviendas que no especificaron si disponen del servicio. Excluye viviendas sin informacion de ocupantes. Excluye viviendas en locales no construidos para habitacion, viviendas moviles y refugios. Excluye viviendas sin informacion de ocupantes.Excluye viviendas en locales no construidos para habitacion, viviendas moviles y refugios.
1003000020	Viviendas particulares habitadas que disponen de excusado o sanitario	Excluye viviendas sin informacion de ocupantes. Excluye viviendas que no especificaron si disponen del servicio. Excluye viviendas en locales no construidos para habitacion, viviendas moviles y refugios.
1003000021	Viviendas particulares habitadas que disponen de television	Excluye viviendas en locales no construidos para habitacion, viviendas moviles y refugios. Excluye viviendas sin informacion de ocupantes.
1003000022	Viviendas particulares habitadas que disponen de refrigerador	Excluye viviendas sin informacion de ocupantes.Excluye viviendas en locales no construidos para habitacion, viviendas moviles y refugios.
1003000023	Viviendas particulares habitadas que disponen de lavadora	Excluye viviendas sin informacion de ocupantes. Excluye viviendas en locales no construidos para habitacion, viviendas moviles y refugios.
1003000024	Viviendas particulares habitadas que disponen de computadora	Excluye viviendas sin informacion de ocupantes. Excluye viviendas en locales no construidos para habitacion, viviendas moviles y refugios
1005000027	poblacion de 5 y mas aÃ±os con primaria	Se refiere a la poblacion de 5 aÃ±os y mas con algun grado aprobado en primaria, incluyendo el no especificado.
1005000028	Hombres de 5 y mas aÃ±os con primaria	Se refiere a la poblacion de 5 aÃ±os y mas con algun grado aprobado en primaria, incluyendo el no especificado.
1005000029	Mujeres de 5 y mas aÃ±os con primaria	Se refiere a la poblacion de 5 aÃ±os y mas con algun grado aprobado en primaria, incluyendo el no especificado.
1002000014	poblacion en hogares	no hay nota
1002000015	poblacion en hogares familiares	no hay nota
1002000016	poblacion en hogares no familiares	no hay nota
1002000017	poblacion en hogares de tipo no especificado	no hay nota
1002000018	Hogares	no hay nota
1002000019	Hogares con jefatura masculina	no hay nota
1002000020	Hogares con jefatura femenina	no hay nota
1002000021	TamaÃ±o promedio de los hogares	no hay nota
1002000022	TamaÃ±o promedio de los hogares con jefe hombre	no hay nota
1002000023	TamaÃ±o promedio de los hogares con jefe mujer	no hay nota
1002000013	Promedio de hijos nacidos vivos de las mujeres de 12 y mas aÃ±os	no hay nota
1002000001	poblacion total	no hay nota
1002000002	poblacion total hombres	no hay nota
1002000003	poblacion total mujeres	no hay nota
1003000003	Ocupantes en casa independiente	no hay nota
1003000004	Ocupantes en departamento en edificio	no hay nota
1003000005	Ocupantes en vivienda o cuarto en vecindad	no hay nota
1003000006	Ocupantes en vivienda o cuarto en azotea	no hay nota
1003000007	Ocupantes en local no construido para habitacion	no hay nota
1003000008	Ocupantes en vivienda movil	no hay nota
1003000009	Ocupantes en refugio	no hay nota
1003000010	Ocupantes en viviendas particulares de clase no especificada	no hay nota
1003000012	Viviendas particulares habitadas con 1 a 4 ocupantes	no hay nota
1003000013	Viviendas particulares habitadas con 5 a 8 ocupantes	no hay nota
1003000014	Viviendas particulares habitadas con 9 y mas ocupantes	no hay nota
1003000015	Promedio de ocupantes en viviendas particulares habitadas	no hay nota
1005000039	poblacion de 5 y mas aÃ±os que habla lengua indigena	no hay nota
1005000040	poblacion de 5 y mas aÃ±os que no habla lengua indigena	no hay nota
1005000041	poblacion de 5 y mas aÃ±os que no especifico si habla lengua indigena	no hay nota
1002000041	Porcentaje de las personas de 15 o mas aÃ±os alfabetas	no hay nota
1002000042	Porcentaje de las personas de 6 a 24 aÃ±os que van a la escuela	no hay nota
1005000001	poblacion de 6 y mas aÃ±os	no hay nota
1005000002	poblacion total de 6 y mas aÃ±os que sabe leer y escribir	no hay nota
1005000003	Hombres de 6 y mas aÃ±os que saben leer y escribir	no hay nota
1005000004	Mujeres de 6 y mas aÃ±os que saben leer y escribir	no hay nota
1005000005	poblacion total de 6 y mas aÃ±os que no sabe leer y escribir	no hay nota
1005000006	Hombres de 6 y mas aÃ±os que no saben leer y escribir	no hay nota
1005000030	poblacion de 5 y mas aÃ±os con instrucciÃ³n posprimaria	Se refiere a la poblacion de 5 aÃ±os y mas con algun grado aprobado en secundaria o equivalente, estudios tÃ©cnicos con primaria terminada, educaciÃ³n media superior y educaciÃ³n superior.
1005000031	Hombres de 5 y mas aÃ±os con instrucciÃ³n posprimaria	Se refiere a la poblacion de 5 aÃ±os y mas con algun grado aprobado en secundaria o equivalente, estudios tÃ©cnicos con primaria terminada, educaciÃ³n media superior y educaciÃ³n superior.
1005000032	Mujeres de 5 y mas aÃ±os con instrucciÃ³n posprimaria	Se refiere a la poblacion de 5 aÃ±os y mas con algun grado aprobado en secundaria o equivalente, estudios tÃ©cnicos con primaria terminada, educaciÃ³n media superior y educaciÃ³n superior.
1005000037	poblacion de 18 aÃ±os y mas con posgrado	Se refiere a la poblacion de 18 y mas aÃ±os con algun grado en MaestrÃ­a y Doctorado.
1005000007	Mujeres de 6 y mas aÃ±os que no saben leer y escribir	no hay nota
1005000013	Hombres de 5 y mas aÃ±os que asisten a la escuela	no hay nota
1005000014	Mujeres de 5 y mas aÃ±os que asisten a la escuela	no hay nota
1005000015	poblacion de 5 y mas aÃ±os que no asiste a la escuela	no hay nota
1005000016	Hombres de 5 y mas aÃ±os que no asisten a la escuela	no hay nota
1005000017	Mujeres de 5 y mas aÃ±os que no asisten a la escuela	no hay nota
1005000018	poblacion de 5 y mas aÃ±os que no especifico si asiste a la escuela	no hay nota
1005000019	Hombres de 5 y mas aÃ±os que no especifican si asisten a la escuela	no hay nota
1005000020	Mujeres de 5 y mas aÃ±os que no especifican si asisten a la escuela	no hay nota
1005000021	poblacion de 5 y mas aÃ±os sin escolaridad	no hay nota
1005000022	Hombres de 5 y mas aÃ±os sin escolaridad	no hay nota
1005000023	Mujeres de 5 y mas aÃ±os sin escolaridad	no hay nota
1005000024	poblacion de 5 y mas aÃ±os con preescolar	no hay nota
1005000025	Hombres de 5 y mas aÃ±os con preescolar	no hay nota
1005000026	Mujeres de 5 y mas aÃ±os con preescolar	no hay nota
1005000033	poblacion de 5 y mas aÃ±os con instrucciÃ³n no especificada	no hay nota
1005000034	Hombres de 5 y mas aÃ±os con instrucciÃ³n no especificada	no hay nota
1005000035	Mujeres de 5 y mas aÃ±os con instrucciÃ³n no especificada	no hay nota
1005000036	poblacion de 18 aÃ±os y mas con nivel profesional	no hay nota
1005000038	Grado promedio de escolaridad de la poblacion de 15 y mas aÃ±os	no hay nota
1004000001	poblacion derechohabiente a servicios de salud	no hay nota
1004000002	poblacion derechohabiente a servicios de salud del IMSS	no hay nota
1004000003	poblacion derechohabiente a servicios de salud del ISSSTE	no hay nota
1004000004	poblacion derechohabiente a servicios de salud en PEMEX, SEDENA y/o SEMAR	no hay nota
1004000005	poblacion derechohabiente a servicios de salud en otra instituciÃ³n	no hay nota
1004000006	poblacion sin derechohabiencia a servicios de salud	no hay nota
1004000007	poblacion que no especifico su condiciÃ³n de derechohabiente	no hay nota
1004000008	poblacion derechohabiente a servicios de salud hombres	no hay nota
1004000009	poblacion derechohabiente a servicios de salud mujeres	no hay nota
\.


--
-- TOC entry 2024 (class 0 OID 0)
-- Dependencies: 172
-- Name: indicadores_id_indicador_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('indicadores_id_indicador_seq', 1005000042, false);


--
-- TOC entry 2004 (class 0 OID 42993)
-- Dependencies: 181
-- Data for Name: municipio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY municipio (id_municipio, id_entidad, descripcion) FROM stdin;
1	1	Total Nacional
\.


--
-- TOC entry 2025 (class 0 OID 0)
-- Dependencies: 180
-- Name: municipio_id_municipio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('municipio_id_municipio_seq', 2, false);


--
-- TOC entry 1998 (class 0 OID 42959)
-- Dependencies: 175
-- Data for Name: tema1; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tema1 (id_tema1, descripcion) FROM stdin;
1	Hogares y Vivienda
2	Sociedad y Gobierno
\.


--
-- TOC entry 2026 (class 0 OID 0)
-- Dependencies: 174
-- Name: tema1_id_tema1_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tema1_id_tema1_seq', 3, false);


--
-- TOC entry 2000 (class 0 OID 42967)
-- Dependencies: 177
-- Data for Name: tema2; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tema2 (id_tema2, id_tema1, descripcion) FROM stdin;
1	1	Hogares
2	1	Natalidad y fecundidad
3	1	PoblaciÃ³n
4	1	Vivienda y urbanizaciÃ³n
5	2	Cultura
6	2	EducaciÃ³n
7	2	Salud
\.


--
-- TOC entry 2027 (class 0 OID 0)
-- Dependencies: 176
-- Name: tema2_id_tema2_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tema2_id_tema2_seq', 8, false);


--
-- TOC entry 2002 (class 0 OID 42980)
-- Dependencies: 179
-- Data for Name: tema3; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tema3 (id_tema3, id_tema2, descripcion) FROM stdin;
1	1	Caracteristicas de los hogares
2	2	Natalidad
3	3	Distribucion por edad y sexo
4	3	Volumen y crecimiento
5	4	Caracteristicas de las viviendas
6	4	Servicios y bienes en las viviendas
7	5	CaracterÃ­sticas culturales de la poblaciÃ³n
8	6	Caracteristica educativas de la poblacion
9	7	Derechohabiencia y uso de servicios de salud
\.


--
-- TOC entry 2028 (class 0 OID 0)
-- Dependencies: 178
-- Name: tema3_id_tema3_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tema3_id_tema3_seq', 10, false);


--
-- TOC entry 1868 (class 2606 OID 42814)
-- Name: pk_entidad; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY entidad
    ADD CONSTRAINT pk_entidad PRIMARY KEY (id_entidad);


--
-- TOC entry 1870 (class 2606 OID 42871)
-- Name: pk_indicador; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY indicadores
    ADD CONSTRAINT pk_indicador PRIMARY KEY (id_indicador);


--
-- TOC entry 1878 (class 2606 OID 42998)
-- Name: pk_municipio; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY municipio
    ADD CONSTRAINT pk_municipio PRIMARY KEY (id_municipio);


--
-- TOC entry 1872 (class 2606 OID 42964)
-- Name: pk_tema1; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tema1
    ADD CONSTRAINT pk_tema1 PRIMARY KEY (id_tema1);


--
-- TOC entry 1874 (class 2606 OID 42972)
-- Name: pk_tema2; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tema2
    ADD CONSTRAINT pk_tema2 PRIMARY KEY (id_tema2);


--
-- TOC entry 1876 (class 2606 OID 42985)
-- Name: pk_tema3; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tema3
    ADD CONSTRAINT pk_tema3 PRIMARY KEY (id_tema3);


--
-- TOC entry 1882 (class 2606 OID 43010)
-- Name: censo_id_municipio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY censo
    ADD CONSTRAINT censo_id_municipio_fkey FOREIGN KEY (id_municipio) REFERENCES municipio(id_municipio) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1883 (class 2606 OID 43015)
-- Name: censo_id_tema1_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY censo
    ADD CONSTRAINT censo_id_tema1_fkey FOREIGN KEY (id_tema1) REFERENCES tema1(id_tema1) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1884 (class 2606 OID 43020)
-- Name: censo_id_tema2_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY censo
    ADD CONSTRAINT censo_id_tema2_fkey FOREIGN KEY (id_tema2) REFERENCES tema2(id_tema2) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1885 (class 2606 OID 43025)
-- Name: censo_id_tema3_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY censo
    ADD CONSTRAINT censo_id_tema3_fkey FOREIGN KEY (id_tema3) REFERENCES tema3(id_tema3) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1881 (class 2606 OID 42999)
-- Name: municipio_id_entidad_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY municipio
    ADD CONSTRAINT municipio_id_entidad_fkey FOREIGN KEY (id_entidad) REFERENCES entidad(id_entidad) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1879 (class 2606 OID 42973)
-- Name: tema2_id_tema1_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tema2
    ADD CONSTRAINT tema2_id_tema1_fkey FOREIGN KEY (id_tema1) REFERENCES tema1(id_tema1) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1880 (class 2606 OID 42986)
-- Name: tema3_id_tema2_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tema3
    ADD CONSTRAINT tema3_id_tema2_fkey FOREIGN KEY (id_tema2) REFERENCES tema2(id_tema2) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2013 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2014-08-08 14:19:04

--
-- PostgreSQL database dump complete
--

